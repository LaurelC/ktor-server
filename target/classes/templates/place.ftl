<!doctype html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- UIKIT -->
    <link rel="stylesheet" href="/static/css/uikit.min.css">

    <script src="/static/js/uikit.min.js"></script>
    <script src="/static/js/uikit-icons.min.js"></script>

    <!-- Local CSS -->
    <link rel="stylesheet" href="/static/css/styles.css">
</head>

<body>

<nav class="uk-navbar-container" uk-navbar>
    <div class="uk-navbar-center">
        <div class="uk-navbar-center-left"><div>...</div></div>

        <a href="" class="uk-navbar-item uk-logo">
            <h1>
                Sea Fight by LaurelC
            </h1>
        </a>

        <div class="uk-navbar-center-right"><div>...</div></div>
    </div>
</nav>

<div class="uk-child-width-1-2@m uk-child-width-1-1@s uk-text-center " uk-grid>
    <div>
        <div class="uk-card uk-card-default uk-card-body">
            <form method="get" action="/place">

                <fieldset class="uk-fieldset">

                    <h2 class="uk-heading-line"><span>Расстановка кораблей</span></h2>
                    <h3 class="uk-heading-line"><span>${user.name}</span></h3>


                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-select">Корабль</label>
                        <div class="uk-form-controls" uk-tooltip="Выберите размер корабля">
                            <select class="uk-select" id="form-stacked-select" name="size">
                                <#list user.myBoard.getListOfAvailableShips() as shipSize>
                                    <option value="${shipSize}">${shipSize}-мачтовый</option>
                                </#list>
                            </select>
                        </div>
                    </div>


                    <div class="uk-margin" uk-tooltip="Выберите направление корабля">
                        <label><input class="uk-radio" type="radio" name="direction" value="D" checked> Вниз <span uk-icon="arrow-down"></span></label><br>
                        <label><input class="uk-radio" type="radio" name="direction" value="R"> Вправо <span uk-icon="arrow-right" ></span></label>
                    </div>

                    <table class="uk-table uk-table-hover uk-table-small uk-width-1-1 uk-table-shrink uk-table-justify" uk-tooltip="Выберите расположение корабля">

                        <thead>
                        <tr>
                            <th ></th>
                            <#list 1..10 as i>
                                <th class="uk-text-center"><b>${i}</b></th>
                            </#list>
                        </tr>
                        </thead>
                        <tbody>
                        <#list 1..10 as y>
                            <tr>
                                <th class="uk-text-center"><b>${y}</b></th>

                                <#list 1..10 as x>
                                    <#if user.myBoard.getCellStatus(x, y) == "S">
                                        <td class="uk-text-truncate" style="background-color: #1E87F0; border-radius: 1rem">
                                        </td>
                                    <#elseif user.myBoard.getCellStatus(x, y) == "E">
                                        <td class="uk-text-truncate">
                                            <input class="uk-radio" type="radio" name="coordinate" value="${x}_${y}" checked>
                                        </td>
                                    </#if>
                                </#list>

                            </tr>
                        </#list>
                        </tbody>
                    </table>

                        <button class="uk-button uk-button-primary uk-width-1-1" type="submit" uk-tooltip="Подтвердите установку корабля">Подтвердить расположение</button>

                </fieldset>
            </form>
        </div>
    </div>

    <div>
        <div class="uk-card uk-card-default uk-card-body uk-text-left">
            <h2 class="uk-heading-line uk-text-center"><span>Инструкция</span></h2>
            <h3 class="uk-heading-bullet">Выберите корабль для расположения</h3>
            <h3 class="uk-heading-bullet">Выберите направление расположение</h3>
            <h3 class="uk-heading-bullet">Выберите точку расположения расположение</h3>
            <h3 class="uk-heading-bullet">Подтвердите выбор</h3>
        </div>
    </div>
</div>



</body>