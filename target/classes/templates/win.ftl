<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- UIKIT -->
    <link rel="stylesheet" href="/static/css/uikit.min.css">

    <script src="/static/js/uikit.min.js"></script>
    <script src="/static/js/uikit-icons.min.js"></script>

    <!-- Local CSS -->
    <link rel="stylesheet" href="/static/css/styles.css">
</head>

<body>
<div class="uk-container uk-section-primary uk-text-center uk-width-1-1@s uk-height-1-1 uk-position-center">

    <div class="uk-margin">
        <div class="uk-inline uk-card uk-card-primary uk-card-body">
            <h2>Поздравляем с победой, Командир!</h2>
        </div>
    </div>

    <div class="uk-margin">
        <a href="/start">
            <button class="uk-button uk-button-primary">Играть еще</button>
        </a>
    </div>

    <div class="uk-margin">
    </div>
</div>
</body>

</html>