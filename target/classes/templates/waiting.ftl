<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- UIKIT -->
    <link rel="stylesheet" href="/static/css/uikit.min.css">

    <script src="/static/js/uikit.min.js"></script>
    <script src="/static/js/uikit-icons.min.js"></script>

    <!-- Local CSS -->
    <link rel="stylesheet" href="/static/css/styles.css">
</head>

<body>
<div class="uk-container uk-section-primary uk-text-center uk-width-1-1@s">

    <div class="uk-margin">
        <div class="uk-inline uk-card uk-card-secondary uk-card-body">
            Готовых соперников в данный момент нет.
            Подождите на этой странице и время от времени проверяйте появились ли соперники.
        </div>
    </div>

    <form action="/game" method="get">
        <div class="uk-margin">
            <button class="uk-button uk-button-primary" type="submit">Проверить соперников</button>
        </div>
    </form>
</div>
</body>

</html>