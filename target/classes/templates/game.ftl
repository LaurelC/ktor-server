<!doctype html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- UIKIT -->
    <link rel="stylesheet" href="/static/css/uikit.min.css">

    <script src="/static/js/uikit.min.js"></script>
    <script src="/static/js/uikit-icons.min.js"></script>

    <!-- Local CSS -->
    <link rel="stylesheet" href="/static/css/styles.css">
</head>

<body>

<nav class="uk-navbar-container" uk-navbar>
    <div class="uk-navbar-center">
        <div class="uk-navbar-center-left"><div>...</div></div>

        <a href="" class="uk-navbar-item uk-logo">
            <h1>
                Sea Fight
            </h1>
        </a>

        <div class="uk-navbar-center-right"><div>...</div></div>
    </div>
</nav>
<div class="uk-child-width-1-1 uk-grid-small uk-grid-match" uk-grid>
    <div>
        <div class="uk-card uk-card-default uk-card-body">
            <h3 class="uk-card-title">${user.lastAction}</h3>
        </div>
    </div>
</div>
<div class="uk-child-width-1-2@m uk-child-width-1-1@s uk-text-center " uk-grid>

    <div>
        <div class="uk-card uk-card-secondary uk-card-body">

            <form method="get" action="/attack">

                <fieldset class="uk-fieldset">

                    <h2 class="uk-heading-line"><span>Вражеская сторона</span></h2>
                    <h2 class="uk-heading-line"><span>${enemyName}</span></h2>

                    <table class="uk-table uk-table-hover uk-table-small uk-width-1-1 uk-table-shrink uk-table-justify" uk-tooltip="Выберите клетку атаки">

                        <thead>
                        <tr>
                            <th ></th>
                            <#list 1..10 as i>
                                <th class="uk-text-center"><b>${i}</b></th>
                            </#list>
                        </tr>
                        </thead>
                        <tbody>
                        <#list 1..10 as y>
                            <tr>
                                <th class="uk-text-center"><b>${y}</b></th>

                                <#list 1..10 as x>
                                    <#if user.enemyBoard.getCellStatus(x, y) == "X">
                                        <td class="uk-text-truncate" style="background-color: #f0ce6e; border-radius: 1rem">
                                        </td>
                                    <#elseif user.enemyBoard.getCellStatus(x, y) == "E">
                                        <td class="uk-text-truncate">
                                            <#if user.getTurn()>
                                                <input class="uk-radio" type="radio" name="coordinate" value="${x}_${y}" checked>
                                            </#if>
                                        </td>
                                    <#elseif user.enemyBoard.getCellStatus(x, y) == "D">
                                        <td class="uk-text-truncate" style="background-color: #f04062; border-radius: 1rem">
                                        </td>
                                    <#elseif user.enemyBoard.getCellStatus(x, y) == "S">
                                        <td class="uk-text-truncate">
                                            <#if user.getTurn()>
                                                <input class="uk-radio" type="radio" name="coordinate" value="${x}_${y}" checked>
                                            </#if>
                                        </td>
                                    <#elseif user.enemyBoard.getCellStatus(x, y) == "M">
                                        <td class="uk-text-truncate">
                                            M
                                        </td>
                                    </#if>
                                </#list>

                            </tr>
                        </#list>
                        </tbody>
                    </table>
                    <#if user.getTurn()>
                    <button class="uk-button uk-button-primary uk-width-1-1" type="submit" uk-tooltip="Подтвердите атаку">Атаковать клетку</button>

                </fieldset>
            </form>
            <#else>
                </fieldset>
                </form>
                <form action="/game" method="get">
                    <div class="uk-margin">
                        <button class="uk-button uk-button-primary" type="submit">Проверить сходил ли соперник</button>
                    </div>
                </form>
            </#if>
        </div>
    </div>

    <div>
        <div class="uk-card uk-card-default uk-card-body">

                <fieldset class="uk-fieldset">

                    <h2 class="uk-heading-line"><span>Моя сторона</span></h2>
                    <h2 class="uk-heading-line"><span>${user.name}</span></h2>

                    <table class="uk-table uk-table-hover uk-table-small uk-width-1-1 uk-table-shrink uk-table-justify">

                        <thead>
                        <tr>
                            <th ></th>
                            <#list 1..10 as i>
                                <th class="uk-text-center"><b>${i}</b></th>
                            </#list>
                        </tr>
                        </thead>
                        <tbody>
                        <#list 1..10 as y>
                            <tr>
                                <th class="uk-text-center"><b>${y}</b></th>

                                <#list 1..10 as x>
                                    <#if user.myBoard.getCellStatus(x, y) == "S">
                                        <td class="uk-text-truncate" style="background-color: #1E87F0; border-radius: 1rem">
                                        </td>
                                    <#elseif user.myBoard.getCellStatus(x, y) == "E">
                                        <td class="uk-text-truncate">
                                        </td>
                                    <#elseif user.myBoard.getCellStatus(x, y) == "D">
                                        <td class="uk-text-truncate" style="background-color: #f04062; border-radius: 1rem">
                                        </td>
                                    <#elseif user.myBoard.getCellStatus(x, y) == "M">
                                        <td class="uk-text-truncate">
                                            M
                                        </td>
                                    <#elseif user.myBoard.getCellStatus(x, y) == "X">
                                        <td class="uk-text-truncate" style="background-color: #f0ce6e; border-radius: 1rem">
                                        </td>
                                    </#if>
                                </#list>

                            </tr>
                        </#list>
                        </tbody>
                    </table>
                </fieldset>
        </div>
    </div>

</div>

</body>