<#-- @ftlvariable name="data" type="com.laurelc.IndexData" -->
<!doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- UIKIT -->
        <link rel="stylesheet" href="/static/css/uikit.min.css">

        <script src="/static/js/uikit.min.js"></script>
        <script src="/static/js/uikit-icons.min.js"></script>

        <!-- Local CSS -->
        <link rel="stylesheet" href="/static/css/styles.css">
    </head>

    <body uk-scrollspy="target: > div; cls:uk-animation-fade; delay: 500">

    <div class="uk-height-large uk-background-cover uk-light uk-flex" uk-parallax="background-color: #316E9B, #DAA7A3 ;bgy: -200; viewport: 0.5" >

        <h1 class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical" uk-parallax="target: #test-filter; blur: 0,10;">
            Вы готовы к битве, Командир?
        </h1>

    </div>
    <nav class="uk-navbar-container" uk-navbar="boundary-align: true; align: center;">
        <div class="uk-navbar-center">

            <a href="" class="uk-navbar-item uk-logo">
                <h1 class="uk-text-middle">
                    Sea Fight
                </h1>
            </a>

            <div class="uk-navbar-center-right"><div> by LaurelC</div></div>
        </div>
    </nav>
    <div class="uk-height-large uk-background-cover uk-light uk-flex" uk-parallax="bgy: -10; bgx: -100" style="background-image: url('/img/bg.jpg');">

    </div>

    <nav class="uk-navbar-container" uk-navbar="boundary-align: true; align: center;">
        <div class="uk-navbar-center">
            <a href="/start" class="uk-navbar-item uk-logo">
                <h1 class="uk-text-middle">
                    В бой!
                </h1>
            </a>
        </div>
    </nav>
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">

        <ul class="uk-slider-items uk-grid uk-grid-match" uk-height-viewport="offset-top: true; offset-bottom: 30">
            <#list images as image>
            <li class="uk-width-3-4">
                <div class="uk-cover-container">
                    <img src=${image.src} alt="" uk-cover>
                </div>
            </li>
            </#list>
        </ul>

    </div>

    <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>
        <div>
            <div class="uk-card uk-card-default uk-card-body">
                <h3 class="uk-card-title">Написано на Kotlin</h3>
                <p>Серверная часть <a class="uk-link-text" href="https://ktor.io">Ktor</a></p>
                <p>Интерфейс <a class="uk-link-text" href="https://3uikit.ru">Uikit 3</a></p>
            </div>
        </div>
        <div>
            <div class="uk-card uk-card-secondary uk-card-body">
                <h3 class="uk-card-title">Морской бой 2019</h3>
                <p>Сделан учащимся группы М2-18 МГУ ТФ</p>
                <p>Лавровым Александром <a href="https://www.instagram.com/lavrlad/" class="uk-icon-button uk-margin-small-right" uk-icon="instagram"></a></p>
            </div>
        </div>
        <div>
            <div class="uk-card uk-card-default uk-card-body">
                <h3 class="uk-card-title">Отдельное спасибо</h3>
                <p>Саидолиму Джураеву</p>
                <a class="uk-link-text" href="https://yordam.uz/">yordam.uz</a>
            </div>
        </div>
    </div>

    </body>
</html>
