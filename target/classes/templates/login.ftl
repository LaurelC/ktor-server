<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- UIKIT -->
    <link rel="stylesheet" href="/static/css/uikit.min.css">

    <script src="/static/js/uikit.min.js"></script>
    <script src="/static/js/uikit-icons.min.js"></script>

    <!-- Local CSS -->
    <link rel="stylesheet" href="/static/css/styles.css">
</head>

<body>
<div class="uk-container uk-section-primary uk-text-center uk-width-1-1@s">
    <#if error??>
        <div class="uk-margin">
            <input class="uk-input uk-form-danger uk-form-width-medium" type="text" placeholder="form-danger" value="${error}">
        </div>
    </#if>

    <div class="uk-margin">
        <div class="uk-inline uk-card uk-card-secondary uk-card-body">
            Введите логин и пароль (равен логину)
        </div>
    </div>

    <form action="/login" method="post" enctype="application/x-www-form-urlencoded">

        <div class="uk-margin">
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <input class="uk-input" type="text" name="username" placeholder="Логин">
            </div>
        </div>

        <div class="uk-margin">
            <div class="uk-inline">
                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                <input class="uk-input" type="password" name="password" placeholder="Пароль">
            </div>
        </div>

        <div class="uk-margin">
            <button class="uk-button uk-button-primary" type="submit">Войти</button>
        </div>

    </form>
</div>
</body>

</html>