package com.laurelc.seaFight

class Cell(var x: Int, var y: Int) {
    var status = CELL_STATUS_EMPTY
    val id: Int
        get() = (this.y - 1) * BOARD_BOUNDARY_Y_MAX + (this.x - 1)

    override fun toString(): String {
        return "Cell: x: $x, y: $y, status: $status"
    }
}