package com.laurelc.seaFight

class Ship(val size: Int) {
    var cellsId = arrayListOf<Int>()
    var status = SHIP_STATUS_NOT_PLACED


    // При инициализации даем ID клеток корабля значение -1, чтобы отличать установленные корабли
    private fun initShipCells(){
        for(i in 1..this.size){
            this.cellsId.add(-1)
        }
    }


    init {
        initShipCells()
    }


    // Проверка корабля на готовность (если координаты не нулевые)
    private fun checkForReady(): Boolean {
        this.cellsId.forEach {
            if(it == -1) return false
        }

        return true
    }


    // Задание координат клеткам корабля и смена статуса
    fun setShipCellsId(cell: Cell, direction: Char): Boolean {
        if(!this.checkForReady()){
            var tempCell = Cell(cell.x, cell.y)
            if (direction == SHIP_DIRECTION_DOWN) {
                for ( it in 0 until size){
                    cellsId[it] = tempCell.id
                    tempCell.y += 1

                }
            } else if (direction == SHIP_DIRECTION_RIGHT) {
                for ( it in 0 until size){
                    cellsId[it] = tempCell.id
                    tempCell.x += 1
                }
            }

            this.status = SHIP_STATUS_ALIVE
            return true
        }

        return false
    }

}
