package com.laurelc.seaFight

const val CELL_STATUS_EMPTY = 'E'
const val CELL_STATUS_MISSED = 'M'
const val CELL_STATUS_FIRED = 'X'
const val CELL_STATUS_DEAD = 'D'
const val CELL_STATUS_SHIPPED = 'S'

const val SHIP_DIRECTION_RIGHT = 'R'
const val SHIP_DIRECTION_DOWN = 'D'

const val SHIP_STATUS_ALIVE = "Alive"
const val SHIP_STATUS_DEAD = "Dead"
const val SHIP_STATUS_NOT_PLACED = "Not placed"

const val BOARD_BOUNDARY_X_MIN = 1
const val BOARD_BOUNDARY_X_MAX = 10
const val BOARD_BOUNDARY_Y_MIN = 1
const val BOARD_BOUNDARY_Y_MAX = 10

const val USER_LAST_ACTION_FIRED = "Вы попали по вражескому игроку"
const val USER_LAST_ACTION_MISSED = "Вы промахнулись"
const val USER_LAST_ACTION_STARTED = "Игра началась"
const val USER_LAST_ACTION_GET_FIRED = "Соперник попал по вашему кораблю"
const val USER_LAST_ACTION_GET_MISSED = "Соперник промахнулся"

const val COMMAND_FIRE = "fire"