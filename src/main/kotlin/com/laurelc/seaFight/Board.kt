package com.laurelc.seaFight

import kotlin.math.abs

class Board {
    private var cells = arrayListOf<Cell>()
    var ships = arrayListOf<Ship>()

    private fun initShips(){
        for(i in 1..10){
            when {
                i == 10 -> {
                    //4 мачты
                    this.ships.add(Ship(4))
                }
                i >= 8 -> {
                    //3 мачты
                    this.ships.add(Ship(3))
                }
                i >= 5 -> {
                    //2 мачты
                    this.ships.add(Ship(2))
                }
                else -> {
                    //1 мачта
                    this.ships.add(Ship(1))
                }
            }
        }
    }


    // Получаем массив размеров кораблей, доступных для установки
    fun getListOfAvailableShips(): ArrayList<Int> {
        var availableShips = arrayListOf<Int>()

        for(ship in this.ships){
            if (ship.status == SHIP_STATUS_NOT_PLACED){
                availableShips.add(ship.size)
            }
        }

        return availableShips
    }


    // Получаем массив размеров живых кораблей
    fun getListOfAliveShips(): ArrayList<Int>{
        var aliveShips = arrayListOf<Int>()
        for(ship in this.ships){
            if (!this.checkForDeath(ship)){
                aliveShips.add(ship.size)
            }
        }

        return aliveShips
    }


    // Проверка на пересечение или соседство с существующими кораблями
    fun isIntersect(testShip: Ship): Boolean {
        for (testCellId in testShip.cellsId) {
            for (cell in this.cells) {
                if (cell.status == CELL_STATUS_SHIPPED) {
                    if (abs(cell.x - this.cells[testCellId].x) <= 1 && abs(cell.y - this.cells[testCellId].y) <= 1) return true
                }
            }
        }
        return false
    }


    // Making an empty board
    private fun initBoard(){
        for (y in 1..BOARD_BOUNDARY_Y_MAX) for (x in 1..BOARD_BOUNDARY_X_MAX)
            cells.add(Cell(x,y))
    }


    init {
        initBoard()
        initShips()
    }


    // Получение значения статуса заданной клетки
    fun getCellStatus(x: Int, y: Int):Char{
        return this.cells[((y-1) * BOARD_BOUNDARY_Y_MAX) + (x-1)].status
    }


    // Установка значения статуса заданной клетки
    fun setCellStatus(cellId: Int, cellStatus: Char){
        this.cells[cellId].status = cellStatus
    }


    // Проходим по клеткам на доске, на которых расположен данный корабль и проверяем, все ли клетки уничтожены
    private fun checkForDeath(ship: Ship): Boolean {
        for (cellId in ship.cellsId){
            if(this.cells[cellId].status != CELL_STATUS_FIRED && this.cells[cellId].status != CELL_STATUS_DEAD){
                return false
            }
        }

        for (cellID in ship.cellsId){
            this.cells[cellID].status = CELL_STATUS_DEAD
        }

        ship.status = SHIP_STATUS_DEAD

        return true
    }

}