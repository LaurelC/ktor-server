package com.laurelc.seaFight

import com.laurelc.users


class User(val name:String) {
    var myBoard: Board = Board()
    var enemyBoard: Board = Board()
    var isReady: Boolean = false
    var isMyTurn: Boolean = false
    var id: Int = 0;
    var enemyId: Int = 0;
    var lastAction: String = USER_LAST_ACTION_STARTED


    fun clean(){
        this.myBoard = Board()
        this.enemyBoard = Board()
        this.isReady = false
        this.isMyTurn = false
        this.enemyId = 0;
        this.lastAction = USER_LAST_ACTION_STARTED
    }


    // Установка статуса SHIPPED на клетки доски по заданному кораблю
    private fun setBoardCells(ship: Ship){
        for(shipCellId in ship.cellsId){
            this.myBoard.setCellStatus(shipCellId, CELL_STATUS_SHIPPED)
        }

        ship.status = SHIP_STATUS_ALIVE
    }


    // Получение булевского значения очереди
    fun getTurn() = this.isMyTurn


    // Установка кораблей и соответствующих клеток на поле
    fun placeShip(size: Int, cell: Cell, direction: Char): Boolean {

        if ((direction == SHIP_DIRECTION_RIGHT && (cell.x + size - 1) > BOARD_BOUNDARY_X_MAX) ||
            (direction == SHIP_DIRECTION_DOWN && (cell.y + size - 1) > BOARD_BOUNDARY_Y_MAX)){
            return false
        }


        var testShip = Ship(size)
        testShip.setShipCellsId(cell, direction)
        if(this.myBoard.isIntersect(testShip)) return false

        // Кораблей определенного размера 5-size штук
        var count = 5 - size

        when (size) {
            1 -> {
                for (id in 0 until count) if (this.myBoard.ships[id].setShipCellsId(cell, direction)) {
                    setBoardCells(this.myBoard.ships[id])
                    return true
                }
            }
            2 -> {
                for (id in 4 until (4 + count)) if (this.myBoard.ships[id].setShipCellsId(cell, direction)) {
                    setBoardCells(this.myBoard.ships[id])
                    return true
                }
            }
            3 -> {
                for (id in 7 until (7 + count)) if (this.myBoard.ships[id].setShipCellsId(cell, direction)) {
                    setBoardCells(this.myBoard.ships[id])
                    return true
                }
            }
            4 -> if(this.myBoard.ships[9].setShipCellsId(cell, direction)) {
                setBoardCells(this.myBoard.ships[9])
                return true
            }
        }

        return false
    }


    // Функция атаки заданной клетки
    fun attack(attackCell: Cell): Boolean {

        val enemy = users[this.enemyId]

        if(attackCell.x < BOARD_BOUNDARY_X_MIN ||
            attackCell.x > BOARD_BOUNDARY_X_MAX ||
            attackCell.y < BOARD_BOUNDARY_Y_MIN ||
            attackCell.y > BOARD_BOUNDARY_Y_MAX){
            this.isMyTurn = true
            enemy.isMyTurn = false

            return false
        }

        val attackCellId = (attackCell.y - 1) * BOARD_BOUNDARY_Y_MAX + attackCell.x -1

        if(enemy.myBoard.getCellStatus(attackCell.x, attackCell.y) == CELL_STATUS_SHIPPED){
            enemy.myBoard.setCellStatus(attackCellId, CELL_STATUS_FIRED)
            this.enemyBoard.setCellStatus(attackCellId, CELL_STATUS_FIRED)
            this.isMyTurn = true
            enemy.isMyTurn = false
            this.lastAction = USER_LAST_ACTION_FIRED
            users[this.enemyId].lastAction = USER_LAST_ACTION_GET_FIRED



        } else {
            enemy.myBoard.setCellStatus(attackCellId, CELL_STATUS_MISSED)
            this.enemyBoard.setCellStatus(attackCellId, CELL_STATUS_MISSED)
            this.isMyTurn = false
            enemy.isMyTurn = true
            this.lastAction = USER_LAST_ACTION_MISSED
            users[this.enemyId].lastAction = USER_LAST_ACTION_GET_MISSED
        }


        return true
    }
}
