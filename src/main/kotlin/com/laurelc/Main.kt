package com.laurelc

import com.laurelc.controllers.*
import com.laurelc.seaFight.User
import freemarker.cache.ClassTemplateLoader
import io.ktor.application.*
import io.ktor.freemarker.FreeMarker
import io.ktor.http.content.*
import io.ktor.routing.*
import io.ktor.sessions.*

var users = arrayListOf<User>()

data class MySession(val username: String, val id: Int)

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {

  users.add(User("Guest"))

  install(Sessions) {
    cookie<MySession>("COOKIE_NAME")
  }
  install(FreeMarker) {
    templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
  }

  routing {

    // Главная страница
    root()

    // Страница авторизации
    login()

    // Начало и расстановка кораблей
    start()

    // Страница ожидания соперника
    waiting()

    // Страница игры
    game()

    // Результат
    result()

    // Статик
    static("/static") {
      resources("static")
    }
    static("/img") {
      resources("static/images")
    }
  }
}


