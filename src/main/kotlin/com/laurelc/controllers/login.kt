package com.laurelc.controllers

import com.laurelc.MySession
import com.laurelc.seaFight.User
import com.laurelc.users
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.sessions.sessions
import io.ktor.sessions.set

fun Route.login(){
    // Страница авторизации
    get("/login"){
        call.respond(FreeMarkerContent("login.ftl", null))
    }

    // POST-запрос к странице авторизации
    post("/login"){
        val post = call.receiveParameters()
        if (post["username"] != null && post["username"] != "" && post["username"] == post["password"]) {
            println("Вошел ${post["username"]}")
            users.add(User(post["username"].toString()))
            val id = users.lastIndex
            users[id].id = id
            call.sessions.set(MySession(username = post["username"].toString(), id = id))
            call.respondRedirect("/start")
        } else {
            call.respond(FreeMarkerContent("login.ftl", mapOf("error" to "Неверные данные")))
        }
    }
}