package com.laurelc.controllers

import com.laurelc.MySession
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.sessions.get
import io.ktor.sessions.sessions

fun Route.result(){
    // Страница победы
    get("/win"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }

        call.respond(FreeMarkerContent("win.ftl", null))
    }

    // Страница поражения
    get("/lose"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }

        call.respond(FreeMarkerContent("lose.ftl", null))
    }
}