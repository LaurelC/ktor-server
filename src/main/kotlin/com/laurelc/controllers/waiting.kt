package com.laurelc.controllers

import com.laurelc.MySession
import com.laurelc.users
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.sessions.get
import io.ktor.sessions.sessions

fun Route.waiting(){
    // Страница ожидания соперника
    get("/waiting"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }

        users[id].isReady = true

        for (potentialRival in users){
            // Пропускаем гостя и себя
            if(potentialRival.id == 0 || potentialRival.id == id) continue

            // Пропускаем неготовых
            if(!potentialRival.isReady){
                //println("${potentialRival.name} id:${potentialRival.id} enemyId:${potentialRival.enemyId} ready: ${potentialRival.isReady}")
                continue
            }

            // Если мы уже соперники, переходим к игре, Если соперник занят не мной, то пропускаем
            if(potentialRival.enemyId != 0 && potentialRival.enemyId == id){
                //println("aaaa ${potentialRival.name} id:${potentialRival.id} enemyId:${potentialRival.enemyId} ready: ${potentialRival.isReady}")

                // Игрок присоединяется к уже созданной игре, права ходить изначально не у него
                users[id].isMyTurn = false
                call.respondRedirect("/game")
            } else if(potentialRival.enemyId != 0 && potentialRival.enemyId != id) continue

            users[id].enemyId = potentialRival.id
            potentialRival.enemyId = id
            potentialRival.enemyBoard = users[id].myBoard
            users[id].enemyBoard = potentialRival.myBoard

            // В данном блоке инициатор игры, поэтому право первого хода у него
            users[id].isMyTurn = true

            //println("${potentialRival.name} id:${potentialRival.id} enemyId:${potentialRival.enemyId} ready: ${potentialRival.isReady}")

            call.respondRedirect("/game")
        }

        call.respond(FreeMarkerContent("waiting.ftl", null, ""))

    }

}