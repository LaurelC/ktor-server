package com.laurelc.controllers

import com.laurelc.MySession
import com.laurelc.seaFight.Cell
import com.laurelc.users
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.sessions.get
import io.ktor.sessions.sessions

fun Route.start(){
    // Страница начала расстановки кораблей
    get("/start"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }
        call.respond(FreeMarkerContent("place.ftl", mapOf("user" to users[id]), ""))
    }

    
    // GET-запрос расстановки кораблей
    get("/place"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }

        if(users[id].myBoard.getListOfAvailableShips().size == 0){
            println("Игрок ${users[id].name} готов")
            call.respondRedirect("/game")
        }


        val size = call.parameters["size"]!!.toInt()
        val dir = call.parameters["direction"]!!.toCharArray()[0]
        val coordinate = call.parameters["coordinate"]!!.toCharArray()
        var x = ""
        var y = ""

        var ifX = true
        for (i in coordinate){
            if(i == '_'){
                ifX = false
                continue
            }
            if(ifX){
                x += i
            } else {
                y += i
            }
        }

        val cell = Cell(x.toInt(), y.toInt())

        users[id].placeShip(size, cell, dir)
        call.respondRedirect("/start")
        //call.respond(FreeMarkerContent("place.ftl", mapOf("user" to users[id]), ""))
    }
}