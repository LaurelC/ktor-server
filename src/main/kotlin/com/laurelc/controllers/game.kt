package com.laurelc.controllers

import com.laurelc.MySession
import com.laurelc.seaFight.Cell
import com.laurelc.users
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.sessions.get
import io.ktor.sessions.sessions

fun Route.game(){
    // Страница игры
    get("/game"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }

        if(users[id].myBoard.getListOfAvailableShips().size != 0){
            users[id].isReady = false
            call.respondRedirect("/start")
        }

        if(users[id].enemyId == 0){
            users[id].isReady = true
            call.respondRedirect("/waiting")
        }

        if(users[id].enemyBoard.getListOfAliveShips().size == 0){
            println("Игрок ${users[id].name} победил игрока ${users[users[id].enemyId].name}")
            users[id].clean()
            call.respondRedirect("/win")
        } else if(users[id].myBoard.getListOfAliveShips().size == 0){
            println("Игрок ${users[id].name} проиграл игроку ${users[users[id].enemyId].name}")
            users[id].clean()
            call.respondRedirect("/lose")
        }

        call.respond(FreeMarkerContent("game.ftl", mapOf("user" to users[id], "enemyName" to users[users[id].enemyId].name), "") )
    }

    // GET-запрос атаки
    get("/attack"){
        val mySession: MySession = call.sessions.get<MySession>() ?: MySession(username = "Guest", id = 0)
        var id: Int = mySession.id

        if(id == 0){
            call.respondRedirect("/login")
        }

        if(users[id].myBoard.getListOfAvailableShips().size != 0){
            users[id].isReady = false
            call.respondRedirect("/start")
        }

        val coordinate = call.parameters["coordinate"]!!.toCharArray()

        var x = ""
        var y = ""

        var ifX = true
        for (i in coordinate){
            if(i == '_'){
                ifX = false
                continue
            }
            if(ifX){
                x += i
            } else {
                y += i
            }
        }

        users[id].attack(Cell(x.toInt(), y.toInt()))

        call.respondRedirect("/game")
    }

}