package com.laurelc.controllers

import com.laurelc.Image
import com.laurelc.MySession
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.sessions.clear
import io.ktor.sessions.sessions

fun Route.root(){
    // Картинки для главной страницы
    var images = arrayListOf<Image>()

    images.add(Image("Chios.jpg"))
    images.add(Image("sinop.jpg"))
    images.add(Image("Shtorm.jpg"))
    images.add(Image("tendera.jpg"))

    // Главная страница
    get("/"){
        call.sessions.clear<MySession>()
        call.respond(FreeMarkerContent("index.ftl", mapOf("images" to images), ""))
    }

    get("/prove"){
      call.respond(FreeMarkerContent("prove.ftl", null))
    }
}
